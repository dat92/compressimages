#!/bin/bash
prefix=cmpr
template=*.jpg

if (( $# != 1)); then
	size=1000;
else 
	size=$1
fi
for i in $template
	do
		echo "Compress image \"$i\" to x$size"
		convert -resize 'x'$size -quality 100 "$i" $prefix"_x$size $i"
	done
